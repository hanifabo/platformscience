# README #

### What is this repository for? ###
This is a code challenge for a job interview


### How to execute the script to test? ###
*
Requirements
Java JDK 8 and above
Maven 3 and above
Java development IDE (Eclipse/Intellij ) or Terminal
internet connection to update dependencies 
*
### How do I get set up? ###

* clone the repository to your local
  git clone https://hanifabo@bitbucket.org/hanifabo/platformscience.git
* 
### Running the tests ###

*  c
1.navigate to the root directory "/platformscience" on your terminal  
2. run <mvn clean install> to get all the dependencies for the project
3. when it's done downloading the dependencies 
4. run  mvn test 
this will run all the test 
5. navigate to the directory 
6. open the target folder select the cucumber-report fold and open the index.html 
that will display the test results

