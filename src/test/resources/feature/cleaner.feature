Feature: platform Science Cleaner

  Scenario: cleaning the room test data
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    And User set "example" as JSON body
    When User makes a "post" request
    Then Calls should should return coords of cleaned area "[1,3]"
    Then User should see the patches count "2"

  Scenario Outline: Verify the coords of cleaned rooms "<roomDetails>" cord
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    And User set "<roomDetails>" as JSON body
    When User makes a "post" request
    Then Calls should should return coords of cleaned area "<results>"
    Then User should see the patches count "<dirts>"
    Then Call should return status
    Examples:
      | roomDetails | results | dirts |
      | room1       | [1, 4]  | 3     |
      | room2       | [5, 1]  | 0     |
      | room4       | [0, 3]  | 2     |
      | room5       | [0, 5]  | 1     |
      | room6       | [0, 5]  | 1     |

  Scenario: Verify error message and status for a post with bad payload
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    When User makes a "post" request
    And User set bad payload as JSON body
    Then User should see the error message "Bad Request"
    Then User should see the error status "400"

  Scenario: Verify error message and status for a post without payload
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    When User makes a "post" request
    And User set empty as JSON body
    Then User should see the response error message "Request body is missing"

  Scenario: Verify error and status for a post without payload
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    When User makes a "post" request
    And User set empty as JSON body
    Then User should see the error message "Bad Request "
    Then User should see the error status "400"

  Scenario Outline: Verify the coords of cleaned rooms "<roomDetails>" cord
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    And User set "<roomDetails>" as JSON body
    When User makes a "post" request
    Then Calls should should return coords of cleaned area "<results>"
    Then User should see the patches count "<dirts>"
    Then Update assertions report
    Examples:
      | roomDetails | results | dirts |
      | room6       | [0, 5]  | 1     |

  Scenario Outline: Verify the response error message
    Given Set the application base Uri "cleanUrl"
    And Set the application basePath "v1/cleaning-sessions"
    And Set the content Type "JSON"
    And User set "<roomDetails>" as JSON body
    When User makes a "post" request
    Then User should see the response error message "Failed to read HTTP message"
    Then User should see the error status "400"
    And Update assertions report
    Examples:
      | roomDetails | results | dirts |
      | room6       | [0, 5]  | 1     |


