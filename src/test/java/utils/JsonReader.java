package utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import java.io.*;

public class JsonReader {
    public static FileInputStream fis;
    public static InputStreamReader in;
    public static BufferedReader bReader;
    public static StringBuilder sb;
    public static String json = "";
    public static JSONObject object = null;
    public static JSONParser parser = null;
    public static Object obj;
    public static File filepath = new File(System.getProperty("user.dir"), "testdata.json");
    public static String line = null;
    public static Writer output = null;

    public static String getPayLoadData(String data) {
        JSONObject object = null;
        String store = null;
        parser = new JSONParser();

        try {
            fis = new FileInputStream(filepath);
            in = new InputStreamReader(fis);
            bReader = new BufferedReader(in);
            sb = new StringBuilder();

            while ((line = bReader.readLine()) != null) {
                sb.append(line);
            }
            json = sb.toString();

            object = (JSONObject) parser.parse(json);

            try {
                store = object.get(data).toString();
            } catch (NullPointerException e) {
                Assert.fail("Provided address: " + data + "  was not found in the listed addresses provided in \n" + filepath);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());

        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        return store;
    }

    public static void writeJsonObject(String objectKey, String objectValue) {
        JSONObject object = null;
        String password = null;
        output = null;
        parser = new JSONParser();

        try {
            fis = new FileInputStream(filepath);
            in = new InputStreamReader(fis);
            bReader = new BufferedReader(in);
            sb = new StringBuilder();

            while ((line = bReader.readLine()) != null) {
                sb.append(line);
            }
            json = sb.toString();

            object = (JSONObject) parser.parse(json);

            object.put(objectKey, objectValue);
            output = new BufferedWriter(new FileWriter(filepath));
            output.write(object.toString());

        } catch (FileNotFoundException e) {
            Assert.fail(e.getMessage());

        } catch (IOException e) {

            Assert.fail(e.getMessage());
        } catch (ParseException e) {
            Assert.fail(e.getMessage());

        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
