package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import utils.JsonReader;


import java.util.Map;

public class RequestStepdefs {
    public static RequestSpecBuilder builder = new RequestSpecBuilder();
    public static RequestSpecification request = RestAssured.given();
    public static ResponseOptions<Response> response;

    public ResponseOptions<Response> executApi(String method) {

        builder.setContentType(ContentType.JSON);
        RequestSpecification respec = builder.build();
        request.spec(respec);

        if (method.equalsIgnoreCase("Get")) {
            return request.get();
        } else if (method.equalsIgnoreCase("POST")) {
            return request.post();
        } else if (method.equalsIgnoreCase("delete")) {
            return request.delete();
        } else if (method.equalsIgnoreCase("PUT")) {
            return request.put();
        } else throw new RuntimeException("The provided request type is not supported");

    }

    public ResponseOptions<Response> calculateCleaning(Map<String, Object> body, String metho) {
        builder.setBody(body);
        return executApi(metho).getBody().jsonPath().get("");

    }

    @Given("^Set the application base Uri \"([^\"]*)\"$")
    public void set_the_application_baseURL(String baseUri) throws Throwable {
        String ur = JsonReader.getPayLoadData(baseUri);
        builder.setBaseUri(ur);
    }

    @Given("^Set the application basePath \"([^\"]*)\"$")
    public void set_the_application_basePath(String path) throws Throwable {
        builder.setBasePath(path);
    }

    @Given("^Set the content Type \"([^\"]*)\"$")
    public void set_the_content_Type(String contentType) throws Throwable {
        System.err.println("this is the content type:  " + contentType);
        String contype = "ContentType." + contentType.toUpperCase();
        builder.setContentType(contype);
    }

    @Given("^User makes a \"([^\"]*)\" request with \"([^\"]*)\" as JSON body$")
    public ResponseOptions<Response> userExecuteCall(String request, String objectName) throws Throwable {
        String body = JsonReader.getPayLoadData(objectName);
        System.err.println("this is the object  " + body);
        builder.setBody(body);
        return executApi(request);

    }

    @Given("^User makes a \"([^\"]*)\" request$")
    public ResponseOptions<Response> executeCall(String request) throws Throwable {
        System.err.println(" the call type: " + request);
        response = executApi(request);
        return response;

    }

    @Given("^User set \"([^\"]*)\" as JSON body$")
    public void setJSONBodyFromFile(String objectName) throws Throwable {
        String body = JsonReader.getPayLoadData(objectName);
        System.err.println("this is the object  " + body);
        builder.setBody(body);

    }

    @Given("^User set empty as JSON body$")
    public void setJSONBody() throws Throwable {
        builder.setBody("{}");

    }

    @Given("^User set bad payload as JSON body$")
    public void setJSONpayload() throws Throwable {
        builder.setBody("{ \"roomSize\" : [5, 5], \"coords\" : [1, 2]}");

    }

    @Then("^User should see the post json response$")
    public void assertResponse() {
        String patches = response.getBody().jsonPath().get("patches").toString();
        String coords = response.getBody().jsonPath().get("coords").toString();
        System.out.println("patches: " + patches);
        System.out.println("coords: " + coords);
        System.out.println("coords: " + response.getBody().prettyPrint());
    }

    @Then("^User should see the final position$")
    public void assertPossiton() {
        String patches = response.getBody().jsonPath().get("patches").toString();
        String coords = response.getBody().jsonPath().get("coords").toString();
        Assert.assertEquals(patches, coords);
    }

    @Then("^User should see error message $")
    public void assertPatches(String result) {
        try {
            String patches = response.getBody().jsonPath().get("patches").toString();
            Assert.assertEquals(patches, result);
        } catch (Exception e) {
            // e.printStackTrace();
        }

    }

    @Then("^Call should return status$")
    public void assertStatus() {
        //String patches = response.getBody().jsonPath().get("status").toString();
        // String coords = response.getBody().jsonPath().get("coords").toString();
        //soft.then(patches);
        //System.out.println("patches: "+patches);
        //System.out.println("coords: "+coords);
        //System.out.println("The response: " + response.getBody().prettyPrint());
    }

    @Then("^Update assertions report$")
    public void assertReports() {

    }

    @Then("^Calls should should return coords of cleaned area \"([^\"]*)\"$")
    public void callsShouldShouldReturnCoordsOfCleanedArea(String result) throws Throwable {
        try {
            String coords = response.getBody().jsonPath().get("coords").toString();
            Assert.assertEquals(coords, result);
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    @Then("^User should see the patches count \"([^\"]*)\"$")
    public void userShouldSeeThePatchesCount(String result) throws Throwable {
        try {
            String counts = response.getBody().jsonPath().get("patches").toString();
            Assert.assertEquals(counts, result);
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    @Then("^User should see the error message \"([^\"]*)\"$")
    public void userShouldSeeError(String result) throws Throwable {
        try {
            String error = response.getBody().jsonPath().get("error").toString();
            System.out.println("THE ERROR MESSAGE   " + error);
            Assert.assertEquals(error, result);
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    @Then("^User should see the response error message \"([^\"]*)\"$")
    public void userShouldSeeResponseError(String result) throws Throwable {
        try {
            String error = response.getBody().jsonPath().get("message").toString();
            System.out.println("THE ERROR MESSAGE   " + error);
            Assert.assertEquals(error, result);
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    @Then("^User should see the error status \"([^\"]*)\"$")
    public void userShouldSeeErrorStatus(String result) throws Throwable {
        try {
            String status = response.getBody().jsonPath().get("status").toString();
            System.out.println("THE ERROR status   " + status);
            Assert.assertEquals(status, result);
        } catch (Exception e) {
            //e.printStackTrace();

        }
    }
}