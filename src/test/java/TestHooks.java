import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.util.Properties;

public class TestHooks {

    public static final Properties PROPERTIES = new Properties();

    @Before
    public void setUpEnviorment(Scenario scenario) throws Exception {
        System.err.println("Execution Started for scenario: " + scenario.getName());
    }

    @After
    public void afterScen(Scenario scenario) {
        System.err.println("Execution Complete for scenario: " + scenario.getName());

    }

}
